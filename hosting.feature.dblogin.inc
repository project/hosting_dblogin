<?php

function hosting_dblogin_hosting_feature() {
  $features['dblogin'] = array(
    'title' => t('Database login'),
    'description' => t('Provides link to login to a web-based database manager (e.g., phpmyadmin).'),
    'status' => HOSTING_FEATURE_DISABLED,
    'module' => 'hosting_dblogin',
    'group' => 'optional',
    );
  return $features;
}
